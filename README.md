# useful-gitlab-links

## General

- Code review guidelines: https://docs.gitlab.com/ee/development/code_review.html
- Build phase Develop & test https://about.gitlab.com/handbook/product-development-flow/#build-phase-2-develop--test
- Engineering handbook: https://about.gitlab.com/handbook/engineering/
- GitLab Roulette: https://gitlab-org.gitlab.io/gitlab-roulette/
- Product/Category/Features - https://about.gitlab.com/handbook/product/categories/features (useful while adding labels to issues)

## Package specific

- handbook: https://about.gitlab.com/handbook/engineering/development/ops/package/
- direction: https://about.gitlab.com/direction/package/#letter-from-the-editor 
